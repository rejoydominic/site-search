import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SearchService } from '../service/search.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  @Output() onResult = new EventEmitter<Array<any>>();
  constructor(public searchService: SearchService) { }

  ngOnInit() {
  }

  inputValueChange(event){
    const searchTerms = event.target.value.split(',');
    this.onResult.emit(this.searchService.search(searchTerms));
  }
}
