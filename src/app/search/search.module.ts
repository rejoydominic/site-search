import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResultComponent } from './search-result/search-result.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { SearchComponent } from './search.component';
import { SearchService } from './service/search.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ SearchComponent, SearchResultComponent, SearchBoxComponent],
  exports:      [ SearchComponent ],
  providers: 	[ SearchService ]
})
export class SearchModule { }
