import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchResults = [];
  constructor() { }

  ngOnInit() {
  }

  updateResults(results) {
    this.searchResults = results;
  }

}
