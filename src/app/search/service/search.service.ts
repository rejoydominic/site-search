import { Injectable } from '@angular/core';

@Injectable()
export class SearchService {

  constructor() { }

  /**
   * Returns the combined search results ordered by siteName
   *
   * @param {any} searchTerms
   * @param {any} sites
   * @param {any} categories
   * @returns
   *
   * @memberOf SearchService
   */
  getSearchResults(searchTerms, sites, categories) {
    const mergedList = this
      .mergeListUnique(this.getSiteResults(searchTerms, sites), this.getCategoryResults(searchTerms, sites, categories))
      .sort((a, b) => {
        return a.siteName.localeCompare(b.siteName);
      });
    return mergedList;
  }

  /**
   * Return the search results based on only site name
   *
   * @param {any} searchTerms
   * @param {any} sites
   * @returns
   *
   * @memberOf SearchService
   */
  getSiteResults(searchTerms, sites) {
    // filter sites based on the search terms
    return sites.filter((site) => {
      site.siteName = site.siteName.toLowerCase();
      // reduce through the search terms to find match
      return searchTerms.reduce((isMatch, term) => {
        // check if the string exists in siteName
        return isMatch ? true : (site.siteName.indexOf(term.toLowerCase()) > -1);
      }, false);
    });
  }

  /**
   * Returns the search results based on only category
   *
   * @param {any} searchTerms
   * @param {any} sites
   * @param {any} categories
   * @returns
   *
   * @memberOf SearchService
   */
  getCategoryResults(searchTerms, sites, categories) {
    const matchingCategories = categories.filter((category) => {
      category.description = category.description.toLowerCase();
      return searchTerms.reduce((isMatch, term) => {
        return isMatch ? true : (category.description.indexOf(term.toLowerCase()) > -1);
      }, false);
    }).map((category) => {
      return category.id;
    });

    // check if any category is matching
    if (matchingCategories.length > 0) {
      // find sites with matching categories
      return sites.filter((site) => {
        const siteCategoryMatchList = site.categoryIds
          .filter((category) => {
            return matchingCategories.indexOf(category) > -1;
          });
        return siteCategoryMatchList.length > 0;
      });
    } else {
      // return without searching in sites as no category match is found
      return [];
    }
  }

  /**
   * Merge two arrays without duplicates
   *
   * @param {any} firstList
   * @param {any} secondList
   * @returns
   *
   * @memberOf SearchService
   */
  mergeListUnique(firstList, secondList) {
    const mergedList = firstList.reduce((newList, firstListItem) => {
      const newItem = newList.find((item) => {
        return JSON.stringify(firstListItem) === JSON.stringify(item);
      });
      if (!newItem) {
        newList = [...newList, firstListItem];
      }
      return newList;
    }, secondList );
    return mergedList;
  }

  search(searchTerms){
    return this.getSearchResults(searchTerms, this.getAllSites(), this.getAllCategories())
  }

  /**
   * Mock data for category
   *
   * @returns List of categories
   *
   * @memberOf SearchService
   */
  getAllCategories() {
    return [
      {
        id: 1,
        description: 'Arts & Entertainment'
      },
      {
        id: 2,
        description: 'Automotive'
      },
      {
        id: 3,
        description: 'Business'
      },
      {
        id: 4,
        description: 'Careers'
      }
    ];
  }

  /**
   * Mock data for sites
   *
   * @returns List of all sites
   *
   * @memberOf SearchService
   */
  getAllSites() {
    return [
      {
        'id': 1,
        'siteName': 'SurferMag',
        'siteUrl': 'www.surfermag.com',
        'description': 'This is the description for SurferMag',
        'categoryIds': [2]
      },
      {
        'id': 2,
        'siteName': 'Ebay',
        'siteUrl': 'www.ebay.com.au',
        'description': 'This is the description for ebay',
        'categoryIds': [1]
      },
      {
        'id': 3,
        'siteName': 'Robs UI Tips',
        'siteUrl': 'www.awesomeui.com.au',
        'description': 'This is the description for the best site in the world. It is the best:)',
        'categoryIds': [4, 3]
      },
      {
        'id': 4,
        'siteName': 'Table Tennis Tips - How to not come runners up',
        'siteUrl': 'www.ttt.com',
        'description': 'This is the description for Table Tennis Tips',
        'categoryIds': [1, 2, 3, 4]
      }
    ];
  }

}
