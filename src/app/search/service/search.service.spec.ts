import { TestBed, inject } from '@angular/core/testing';

import { SearchService } from './search.service';

describe('SearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchService]
    });
  });

  it('should have search service injected', inject([SearchService], (service: SearchService) => {
    expect(service).toBeTruthy();
  }));


  it('should return site results based on siteName when getSiteResults() is invoked', () => {
    const searchService = new SearchService();
    const _searchTerms = ['one', 'two'];
    const _sites_initial = [{ siteName: 'one' }, { siteName: 'two' }, { siteName: 'three' }];
    const _sites_result = [{ siteName: 'one' }, { siteName: 'two' }];
    expect(searchService.getSiteResults(_searchTerms, _sites_initial)).toEqual(_sites_result);
  });

  it('should return site results if the siteName contains the search term', () => {
    const searchService = new SearchService();
    const _searchTerms = ['one', 'two'];
    const _sites_initial = [{ siteName: 'one-data' }, { siteName: 'data-two' }, { siteName: 'data-two-data' }, { siteName: 'three' }];
    const _sites_result = [{ siteName: 'one-data' }, { siteName: 'data-two' }, { siteName: 'data-two-data' }];

    expect(searchService.getSiteResults(_searchTerms, _sites_initial)).toEqual(_sites_result);
  });


  it('should return sites based on categories when getCategoryResults() is invoked ', () => {
    const searchService = new SearchService();
    const _searchTerms = ['one', 'two'];
    const _categories = [{ id: 1, description: 'one' }, { id: 2, description: 'two' }, { id: 3, description: 'three' }];
    const _sites_initial = [{ categoryIds: [1] }, { categoryIds: [1, 2] }, { categoryIds: [3] }];
    const _sites_result = [{ categoryIds: [1] }, { categoryIds: [1, 2] }];

    expect(searchService.getCategoryResults(_searchTerms, _sites_initial, _categories)).toEqual(_sites_result);
  });

  it('should return sites based on categories when getCategoryResults() is invoked with search matching multiple sites', () => {
    const searchService = new SearchService();
    const _searchTerms = ['two'];
    const _categories = [{ id: 1, description: 'one-one' }, { id: 2, description: 'two-three' }, { id: 3, description: 'three-two' }];
    const _sites_initial = [{ categoryIds: [1] }, { categoryIds: [1, 2] }, { categoryIds: [3] }];
    const _sites_result = [{ categoryIds: [1, 2] }, { categoryIds: [3] }];

    expect(searchService.getCategoryResults(_searchTerms, _sites_initial, _categories)).toEqual(_sites_result);
  });


  it('should return search results based on siteName and category when getSearchResults() is invoked', () => {
    const searchService = new SearchService();
    const _searchTerms_1 = ['one', 'two'];
    const _searchTerms_2 = ['two', 'three'];
    const _searchTerms_3 = ['two'];
    const _searchTerms_4 = ['not-available'];
    const _categories = [
      { id: 1, description: 'one' },
      { id: 2, description: 'two' },
      { id: 3, description: 'three' }
    ];
    const _sites_initial = [
      { siteName: 'one', categoryIds: [1] },
      { siteName: 'two', categoryIds: [1, 2] },
      { siteName: 'three', categoryIds: [3] }
    ];
    const _sites_result_1 = [
      { siteName: 'one', categoryIds: [1] },
      { siteName: 'two', categoryIds: [1, 2] }
    ];
    const _sites_result_2 = [
      { siteName: 'two', categoryIds: [1, 2] },
      { siteName: 'three', categoryIds: [3] }
    ];
    const _sites_result_3 = [
      { siteName: 'two', categoryIds: [1, 2] }
    ];
    const _sites_result_4 = [];

    expect(searchService.getSearchResults(_searchTerms_1, _sites_initial, _categories))
      .toEqual(_sites_result_1.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
    expect(searchService.getSearchResults(_searchTerms_2, _sites_initial, _categories))
      .toEqual(_sites_result_2.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
    expect(searchService.getSearchResults(_searchTerms_3, _sites_initial, _categories))
      .toEqual(_sites_result_3.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
    expect(searchService.getSearchResults(_searchTerms_4, _sites_initial, _categories))
      .toEqual(_sites_result_4.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
  });

  it('should return search results based on siteName and category when getSearchResults() is invoked with mixed category', () => {
    const searchService = new SearchService();
    const _searchTerms_1 = ['one', 'two'];
    const _searchTerms_2 = ['two', 'three'];
    const _searchTerms_3 = ['two'];
    const _searchTerms_4 = ['not-available'];
    const _categories = [
      { id: 1, description: 'one' },
      { id: 2, description: 'two' },
      { id: 3, description: 'three' }
    ];
    const _sites_initial = [
      { siteName: 'one', categoryIds: [3] },
      { siteName: 'two', categoryIds: [1] },
      { siteName: 'three', categoryIds: [1, 2, 3] }
    ];
    const _sites_result_1 = [
      { siteName: 'one', categoryIds: [3] },
      { siteName: 'two', categoryIds: [1] },
      { siteName: 'three', categoryIds: [1, 2, 3] }
    ];
    const _sites_result_2 = [
      { siteName: 'one', categoryIds: [3] },
      { siteName: 'two', categoryIds: [1] },
      { siteName: 'three', categoryIds: [1, 2, 3] }
    ];
    const _sites_result_3 = [
      { siteName: 'two', categoryIds: [1] },
      { siteName: 'three', categoryIds: [1, 2, 3] }
    ];
    const _sites_result_4 = [];

    expect(searchService.getSearchResults(_searchTerms_1, _sites_initial, _categories))
      .toEqual(_sites_result_1.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
    expect(searchService.getSearchResults(_searchTerms_2, _sites_initial, _categories))
      .toEqual(_sites_result_2.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
    expect(searchService.getSearchResults(_searchTerms_3, _sites_initial, _categories))
      .toEqual(_sites_result_3.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
    expect(searchService.getSearchResults(_searchTerms_4, _sites_initial, _categories))
      .toEqual(_sites_result_4.sort((a, b) => {return a.siteName.localeCompare(b.siteName); }));
  });


  it('should merge two lists without duplicates - mergeListUnique', () => {
    const searchService = new SearchService();
    const _firstList = [ {name: 'a'}, {name: 'b'}, {name: 'c'}];
    const _secondList = [ {name: 'b'}, {name: 'c'}, {name: 'd'}];
    const _result = [{name: 'b'}, {name: 'c'}, {name: 'd'}, {name: 'a'}];

    expect(searchService.mergeListUnique(_firstList, _secondList)).toEqual(_result);

  });



});
